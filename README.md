# BrightCodes Android Sample App

## Features 
- Easy to use
- Optimized 
- Customizable

## SDK Support
The minimum supported Android SDK version is 21 inclusive.

## NDK Support
This version of the app was tested against **NDK version R17c**

## Installing
To add the BrightCodes Android SDK to an Android project, add the following dependency to your module's build.gradle:

```
dependencies {
  implementation 'com.brightcodes:brightcodes:X.X.X-prod'
}
```

## Usage

Prerequisites.
    Open Application Settings and set "Show If Recognized" value to "none"


```
    public static final String HEADER_CLIENT = "client";
    public static final String HEADER_UID = "uid";
    public static final String HEADER_ACCESS_TOKEN = "access-token";

    public static final int REQUEST_CODE_RECOGNIZE_TO_DATA = 2;

...

// Prepare Intent using appropriate headers
Intent intent = new Intent(MainActivity.this, CameraActivity.class);
intent.putExtra(HEADER_CLIENT, "...");
intent.putExtra(HEADER_UID, "...");
intent.putExtra(HEADER_ACCESS_TOKEN, "...");

// and launch activity for recognition 
startActivityForResult(intent, REQUEST_CODE_RECOGNIZE_TO_DATA); 

...

// Handle result
@Override
protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_CODE_CAMERA_RECOGNIZE) {
        if (resultCode == RESULT_OK && data != null) {
            Log.i(TAG, "data: " + data.getParcelableExtra("data"));
            Toast.makeText(this, ((Data) data.getParcelableExtra("data")).toString(), Toast.LENGTH_LONG).show();
            ((Data) data.getParcelableExtra("data")).describeContents();
        } else if (data != null) {
            Log.e(TAG, "error: " + ((ErrorData)data.getParcelableExtra("error")).toString());
        }
    }
}
```

## Application Settings
Move to Camera screen and make two long and one short taps at the top right corner of the screen

## Demo 
<link to video>

## Customizations
- White Balance 
- ISO 
- Exposure Time 
- Preview FPS 
- Preview size 
- Focus Distance
- Zoom 

## Change log
*1.0.0*
- Algorithm preview released

## License
<license link>
