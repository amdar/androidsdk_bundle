package com.brightcodes.brightcodessample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.brightcodes.brightcodessample.R;
import com.brightcodes.brightcodessdk.activities.CameraActivity;
import com.brightcodes.brightcodessdk.model.Data;
import com.brightcodes.brightcodessdk.model.ErrorData;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    public static final String HEADER_CLIENT = "client";
    public static final String HEADER_UID = "uid";
    public static final String HEADER_ACCESS_TOKEN = "access-token";

    public static final int REQUEST_CODE_RECOGNIZE_TO_DATA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.cameraImgv).setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, CameraActivity.class);
            addHeaders(intent);
            startActivityForResult(intent, REQUEST_CODE_RECOGNIZE_TO_DATA);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_RECOGNIZE_TO_DATA) {
            if (resultCode == RESULT_OK && data != null) {
                Log.i(TAG, "data: " + data.getParcelableExtra("data"));
//                Toast.makeText(this, ((Data) data.getParcelableExtra("data")).toString(), Toast.LENGTH_LONG).show();

                String url = ((Data) data.getParcelableExtra("data")).getUrl();
                if (url != null) {
                    Intent i = WebActivity.getIntent(this, url);
                    startActivity(i);
                } else {
                    Toast.makeText(this, "Please set product url for showing product page", Toast.LENGTH_LONG).show();
                }

//                ((Data) data.getParcelableExtra("data"))
            } else if (data != null) {
                Log.i(TAG, "data: " + data.getParcelableExtra("error"));
//                ((ErrorData)data.getParcelableExtra("error"));
                Toast.makeText(this, "Error. " + ((ErrorData) data.getParcelableExtra("error")).getMessage(), Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(this, "Result Code: " + requestCode + ", data: " + (data == null), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Request Code: " + requestCode, Toast.LENGTH_LONG).show();
        }

    }

    private void addHeaders(Intent intent) {
        intent.putExtra(HEADER_CLIENT, "zLpj2jkaQzA7jL0qOKhlqw");
        intent.putExtra(HEADER_UID, "demouser1@bright.codes");
        intent.putExtra(HEADER_ACCESS_TOKEN, "KrK_V79NTutLZz7BcPv3Rw");
    }
}
