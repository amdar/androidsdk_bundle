package com.brightcodes.brightcodessample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.brightcodes.brightcodessample.R;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_PERIOD = 2000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        long start = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        long pause = SPLASH_PERIOD - (System.currentTimeMillis() - start);
        if (pause > 0) {
            new Handler().postDelayed(this::showNextScreen, pause);
        } else {
            showNextScreen();
        }
    }

    private void showNextScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}

