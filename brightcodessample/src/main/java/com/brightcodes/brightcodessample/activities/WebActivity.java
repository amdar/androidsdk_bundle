package com.brightcodes.brightcodessample.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.brightcodes.brightcodessample.R;

public class WebActivity extends AppCompatActivity {

    private static final String EXTRA_URL = "url";

    public static Intent getIntent(Context context, String url) {
        Intent i = new Intent(context, WebActivity.class);
        i.putExtra(EXTRA_URL, url);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        String url = getIntent().getStringExtra(EXTRA_URL);
        WebView webView = findViewById(R.id.web_content);
        webView.loadUrl(url);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }
}
